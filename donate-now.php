<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Donate Now </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/darkbox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>
<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/donate-now-banner.png)">
        <div class="container">
            <div class="content">
                <h1 class="title-boxed white-theme pr-5 ">DONATE TO PROTECT OUR FUTURE</h1>
                <p class="col-lg-6 pl-0 pt-3">
                    Together we can fight for human rights everywhere. We can stop globl warming, health and protect our
                    mother nature for our childrens future. Your dnation can transform the lives of millions.
                </p>
            </div>
        </div>
        </div>
    </section>
    <section class="form-container donate-now">
        <div class="container">
            <div class="form-holder row align-items-end ">
                <div class="col-lg-7">
                    <form class="contact-form row">
                        <div class="form-field  col-12">
                            <input id="name" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="name">Name</label>
                        </div>
                        <div class="form-field col-12">
                            <input id="email" class="input-text js-input" type="email" required>
                            <label class="label f-label" for="email">E-mail</label>
                        </div>
                        <div class="form-field  col-12">
                            <input id="number" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="number">Contact Number</label>
                        </div>
                        <div class="form-field col-12">
                            <input id="message" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="message">Address</label>
                        </div>
                        <div class="form-field col-12 row m-0 justify-content-end">
                            <button class="submit-btn agree" type="submit" value=""> Agree </button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-5">
                    <div class=" address ">
                        <h5>CONTACT US </h5>
                        <h6>Proletarian Eco Health Research Foundation</h6>
                        <p class="mb-0">Reg.No : 5/2013/4, Kalloth , Build.No:5 Chenoli Po, </p>
                        <p class="mb-0">Perambra, Kozhikode, 673525.</p>
                        <a class="contact-num" href="">Phone: 0496 2613178, +91- 9497303178 , 9446405636</a>
                        <a class="contact-eamil" href="">Email: ecohealthfoundation@gmail.com</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5 pb-5 mt-4 fund-info">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pr-5">
                    <h2 class="title-boxed dark-theme mb-4 p-0 col-12 pl-2">WHERE YOUR MONEY GOES</h2>
                    <p>Every donation you make supports our work across the world - from helping our research teams
                        expose hidden human rights abuses, to funding vitl activism and campaigns so that we can
                        challenge those in power.</p>
                    <p>We couldn’t do any of this work without your support.</p>
                </div>
                <div class="col-lg-5 row m-0 justify-content-end">
                    <ul class="graph">
                        <li><span>40%</span> Research and publications</li>
                        <li><span>20% </span>Support </li>
                        <li><span>10% </span> Governance and other</li>
                        <li><span> 5% </span> Organic farming </li>
                        <li><span> 5% </span> Organic food supply </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/darkbox.js" type="text/javascript"></script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
        // $( '.js-input' ).keyup(function() {
        //     if( $(this).val() ) {
        //         $(this).addClass('not-empty');
        //         alert("da");
        //     } else {
        //         $(this).removeClass('not-empty');
        //     }
        //     });
        jQuery(document).ready(function () {
            var formControl = jQuery(' .form-field  ').find('.input-text');
            var inputElem = jQuery('.form-field').find('.input-text ');
            inputElem.focus(function () {
                jQuery(this).parent().find('.f-label').addClass("not-empty");
            });
            inputElem.blur(function () {
                jQuery(this).parent('.form-field ').removeClass('focus');
            });
            formControl.each(function () {
                var targetItem = jQuery(this).parent();
                if (jQuery(this).val()) {
                    jQuery(targetItem).addClass('not-empty');
                }
            });
            formControl.blur(function () {
                jQuery(this).parent('.form-field ').removeClass('focus');
                if (jQuery(this).val().length == 0) {
                    jQuery(this).parent().find('.f-label').removeClass("not-empty");
                } else {
                    jQuery(this).parent().find('.f-label').addClass("not-empty");
                }
            });
            function init() {
                document.getElementsByClassName("input-text ").value = "";
            }
            init();
        });
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Books </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/darkbox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>

<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/Books-banner.png)">
        <div class="container">
            <div class="content">
                <h1 class="title-boxed white-theme pr-5 mb-3">BOOKS AND PUBLICATIONS</h1>
            </div>
        </div>
        </div>
    </section>
    <section class="books mt-5 ">
        <div class="container">
            <!-- book holder  -->
            <div class="book-holder">
            <div class="row book-info-container mt-4 mb-5 pb-5 boder-bottom">
                <div class="col-12">
                    <h2 class="title-boxed-sm mb-3"> 'I' MAKE YOU SICK </h2>
                </div>
                <div class="col-lg-6 row m-lg-0">
                    <div class="col-lg-3 p-lg-0 auther-info">
                        <img class="img-fluid" src="resources/images/books/author1.png" alt="">
                        <h6>Dr. B.M. HEGDE</h6>
                    </div>
                    <div class="col-lg-9">
                        <p class="book-briefing">
                            What we have here is an overview of the entire system of medical care, seen through the
                            wisdom
                            of a man who cares. It is a fascinating book distilling the author's philosophy of life and
                            medicine after many years of professional experience as a senior Cardiologist. This book by
                            Dr.Hegde is a thought provoking challenge to modern medical practice and research.Dr Hegde
                            confronts the issue, and proposes an alternative humanistic attitude to <a href="#"
                                class="view-more">+</a> </p>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <img class="img-fluid" src="resources/images/books/book1.png" alt="">
                </div>
                <div class="col-lg-3 row m-0 align-items-center">
                    <div class="book-info ">
                        <div class="group row">
                            <p class="col-lg-4 meta">Author </p>
                            <p class="col-lg-8  info ">Dr. B.M. HEGDE / SHABIL KRISHNAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Category </p>
                            <p class="col-lg-8  info  ">INTERVIEW </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Publisher </p>
                            <p class="col-lg-8  info ">PROLETARIAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Language </p>
                            <p class="col-lg-8  info  ">MALAYALAM </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Price </p>
                            <p class="col-lg-8 info "> <img class="icon" src="resources/images/icons/rupee-indian.svg"
                                    alt=""> 120.00 </p>
                        </div>

                        <a class="buynow" href="buy.php"> Buy Now</a>
                    </div>
                </div>
            </div>
            </div>
            <!-- book holder  -->
            <div class="book-holder">
            <div class="row book-info-container mt-4 mb-5 pb-5 boder-bottom">
                <div class="col-12">
                    <h2 class="title-boxed-sm mb-3"> SCIENTIFIC ECO FRIENDLY FARMING</h2>
                </div>
                <div class="col-lg-6 row m-lg-0">
                    <div class="col-lg-3 p-lg-0 auther-info">
                        <img class="img-fluid" src="resources/images/books/author2.png" alt="">
                        <h6>DEEPAK SUCHDE</h6>
                    </div>
                    <div class="col-lg-9">
                        <p class="book-briefing">
                            Netueco is the way to evergreen revolution. This is the scientific answer to the life
                            killing chemical farming and pesticides .This book is about Netueco farming and its
                            philosophy. The book portrays the experiences of the visionary Deepak Suchde, disciple of
                            Sri. S. A. Dhabolkar who had made a revolution in the agriculture sector through Prayog
                            Parivar in 1970’s. Food mile is increasing day by day. Rice, wheat and vegetables travel
                            hundreds of<a href="#" class="view-more">+</a> </p>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <img class="img-fluid" src="resources/images/books/book2.png" alt="">
                </div>
                <div class="col-lg-4 row m-0 align-items-center">
                    <div class="book-info">
                        <div class="group row">
                            <p class="col-lg-4 meta">Author </p>
                            <p class="col-lg-8  info">DEEPAK SUCHDE / SHABIL KRISHNAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Category </p>
                            <p class="col-lg-8  info  ">INTERVIEW </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Publisher </p>
                            <p class="col-lg-8  info ">PROLETARIAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Language </p>
                            <p class="col-lg-8  info  ">MALAYALAM </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Price </p>
                            <p class="col-lg-8 info "> <img class="icon" src="resources/images/icons/rupee-indian.svg"
                                    alt=""> 120.00 </p>
                        </div>

                        <a class="buynow" href="buy.php"> Buy Now</a>
                    </div>
                </div>
            </div>
            </div>
            <!-- book holder  -->
            <div class="book-holder">
            <div class="row book-info-container mt-4 mb-5 pb-5 boder-bottom">
                <div class="col-12">
                    <h2 class="title-boxed-sm mb-3">GANDHIʼS LIFE AND PHILOSOPHY</h2>
                </div>
                <div class="col-lg-6 row m-lg-0">
                    <div class="col-lg-3 p-lg-0 auther-info">
                        <img class="img-fluid" src="resources/images/books/author3.png" alt="">
                        <h6>K. ARAVINDAKSHAN</h6>
                    </div>
                    <div class="col-lg-9 ">
                        <p class="book-briefing">
                            Mahatma Gandhi was a profound and original thinker, one of the most influential figures in
                            the history of the twentieth century. His many and varied writings largely respond to the
                            specific challenges he faced throughout his life, and they show his evolving ideas, as well
                            as his deepening spirituality and humanity, over several decades. Drawn from the full range
                            of Gandhi's published work--books, articles, broadcasts the pieces are arranged to un
                            hundreds of<a href="#" class="view-more">+</a> </p>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <img class="img-fluid" src="resources/images/books/book3.png" alt="">
                </div>
                <div class="col-lg-4 row m-0 align-items-center">
                    <div class="book-info">
                        <div class="group row">
                            <p class="col-lg-4 meta">Author </p>
                            <p class="col-lg-8  info">K. ARAVINDAKSHAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Category </p>
                            <p class="col-lg-8  info  ">STUDY </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Publisher </p>
                            <p class="col-lg-8  info ">PROLETARIAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Language </p>
                            <p class="col-lg-8  info  ">MALAYALAM </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Price </p>
                            <p class="col-lg-8 info "> <img class="icon" src="resources/images/icons/rupee-indian.svg"
                                    alt=""> 120.00 </p>
                        </div>

                        <a class="buynow" href="buy.php"> Buy Now</a>
                    </div>
                </div>
            </div>
            </div>
            <!-- book holder  -->
            <div class="book-holder">
            <div class="row book-info-container mt-4 mb-5 pb-5 boder-bottom">
                <div class="col-12">
                    <h2 class="title-boxed-sm mb-3">AGRICULTURE PRINCIPLES TO REGAIN HEALTH</h2>
                </div>
                <div class="col-lg-6 row m-lg-0">
                    <div class="col-lg-3 p-lg-0 auther-info">

                        <h6>K.V. DEYAL, K.V. SIVA PRASAD / SHABIL KRISHNAN</h6>
                    </div>
                    <div class="col-lg-9 ">
                        <p class="book-briefing">
                            Pioneers in the ecological movement in Kerala K.V.-Dayal and Sivaprasad speaks about ecology
                            and its various perspectives. Methods of agriculture, history and tradition of eco systems
                            in Kerala and its pres-ent scenario are discussed in detail. This book gives an insight
                            about need to sustain harmony between man and nature.<a href="#" class="view-more">+</a>
                        </p>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <img class="img-fluid" src="resources/images/books/book4.png" alt="">
                </div>
                <div class="col-lg-4 row m-0 align-items-center">
                    <div class="book-info">
                        <div class="group row">
                            <p class="col-lg-4 meta">Author </p>
                            <p class="col-lg-8  info">K.V. DEYAL, K.V. SIVA PRASAD / SHABIL KRISHNAN</p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Category </p>
                            <p class="col-lg-8  info  ">STUDY </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Publisher </p>
                            <p class="col-lg-8  info ">PROLETARIAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Language </p>
                            <p class="col-lg-8  info  ">MALAYALAM </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Price </p>
                            <p class="col-lg-8 info "> <img class="icon" src="resources/images/icons/rupee-indian.svg"
                                    alt=""> 120.00 </p>
                        </div>

                        <a class="buynow" href="buy.php"> Buy Now</a>
                    </div>
                </div>
            </div>
            </div>
            <!-- book holder  -->
            <div class="book-holder">
            <div class="row book-info-container mt-4 mb-5 pb-5 boder-bottom">
                <div class="col-12">
                    <h2 class="title-boxed-sm mb-3">THE HEART THAT TOUCHES SOIL</h2>
                </div>
                <div class="col-lg-6 row m-lg-0">
                    <div class="col-lg-3 p-lg-0 auther-info">
                        <img class="img-fluid" src="resources/images/books/author4.png" alt="">
                        <h6>Dr. J G ROY / </h6>
                    </div>
                    <div class="col-lg-9 ">
                        <p class="book-briefing">
                            Dr. J .G. Ray an internationally acclaimed soil scientist speaks about the dance of nature
                            in the soil. It is the soil that withhold life on earth. An imbalance in the soil creates
                            imbalance in the environment and chaos in the nature. Scientific perspective and holistic
                            approach of Ray makes this book a treasure of knowledge for students and layman<a href="#"
                                class="view-more">+</a>
                        </p>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <img class="img-fluid" src="resources/images/books/book5.png" alt="">
                </div>
                <div class="col-lg-4 row m-0 align-items-center">
                    <div class="book-info">
                        <div class="group row">
                            <p class="col-lg-4 meta">Author </p>
                            <p class="col-lg-8  info">K.V. DEYAL, K.V. SIVA PRASAD / SHABIL KRISHNAN</p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Category</p>
                            <p class="col-lg-8  info  ">STUDY </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Publisher </p>
                            <p class="col-lg-8  info ">PROLETARIAN </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Language </p>
                            <p class="col-lg-8  info  ">MALAYALAM </p>
                        </div>
                        <div class="group row">
                            <p class="col-lg-4 meta">Price </p>
                            <p class="col-lg-8 info "> <img class="icon" src="resources/images/icons/rupee-indian.svg"
                                    alt=""> 120.00 </p>
                        </div>

                        <a class="buynow" href=""> Buy Now</a>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-12 text-center mb-5">
                    <a id="loadMore"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                    <a id="showLess"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                </div>
        </div>
    </section>
    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/darkbox.js" type="text/javascript"></script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
        var numberOfItems = 3;
        $('.book-holder:lt(' + numberOfItems + ')').show();
        $('#loadMore').click(function () {
            numberOfItems = numberOfItems + 5;
            $('.book-holder:lt(' + numberOfItems + ')').show();
            $('#loadMore').css("display", "none");
            $('#showLess').css("display", "block");
        });
        $('#showLess').click(function () {
            numberOfItems = numberOfItems - 5;
            $('.book-holder').not(':lt(' + numberOfItems + ')').hide();
            $('#loadMore').css("display", "block");
            $('#showLess').css("display", "none");
        });
    </script>
</body>

</html>
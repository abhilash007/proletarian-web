<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Book Name </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/darkbox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>

<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/Books-banner.png)">
        <div class="container">
            <div class="content">
                <h1 class="title-boxed white-theme pr-5 mb-3">BOOKS AND PUBLICATIONS</h1>
            </div>
        </div>
        </div>
    </section>
    <section class="form-container">
        <div class="container">
            <div class="row align-items-center  pb-5 mb-5 ">
                <div class="col-lg-3  ">
                    <img src="resources/images/books/book-thumb-1.png" alt="book Name" class="img-fluid book-thumb">
                </div>
                <div class="col-lg-7">
                    <form class="contact-form row">
                        <div class="form-field  col-12">
                            <input id="name" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="name">Name</label>
                        </div>
                        <div class="form-field col-12">
                            <input id="email" class="input-text js-input" type="email" required>
                            <label class="label f-label" for="email">E-mail</label>
                        </div>
                        <div class="form-field  col-12">
                            <input id="number" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="number">Contact Number</label>
                        </div>
                        <div class="form-field col-12">
                            <input id="message" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="message">Address</label>
                        </div>
                        <div class="form-field col-12">
                            <input id="message" class="input-text js-input" type="text" required>
                            <label class="label f-label" for="message">Leave your messege</label>
                        </div>
                        <div class="form-field col-12 row m-0 justify-content-end">
                            <button class="submit-btn agree" type="submit" value=""> Agree</button>
                        </div>

                    </form>

                </div>



            </div>




        </div>
    </section>

    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/darkbox.js" type="text/javascript"></script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
         jQuery(document).ready(function () {
            var formControl = jQuery(' .form-field  ').find('.input-text');
            var inputElem = jQuery('.form-field').find('.input-text ');
            inputElem.focus(function () {
                jQuery(this).parent().find('.f-label').addClass("not-empty");
            });
            inputElem.blur(function () {
                jQuery(this).parent('.form-field ').removeClass('focus');
            });
            formControl.each(function () {
                var targetItem = jQuery(this).parent();
                if (jQuery(this).val()) {
                    jQuery(targetItem).addClass('not-empty');
                }
            });
            formControl.blur(function () {
                jQuery(this).parent('.form-field ').removeClass('focus');
                if (jQuery(this).val().length == 0) {
                    jQuery(this).parent().find('.f-label').removeClass("not-empty");
                } else {
                    jQuery(this).parent().find('.f-label').addClass("not-empty");
                }
            });
            function init() {
                document.getElementsByClassName("input-text ").value = "";
            }
            init();
        });
    </script>
    </script>
</body>

</html>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>
<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="banner-outer ">
        <div class="hero ">
            <div class="flexslider">
                <ul class="slides ">
                    <li style="background-image: url(resources/images/slides/slide-1.png);">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="row m-0  slider-text-outer ">
                                <div class="slider-text">
                                    <div class="banner-content row m-0 justify-content-center">
                                        <div class="col-md-6">
                                            <h1>
                                                FOOD IN OUR LAND <br>
                                                HEALTH IN OUR HAND
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="background-image: url(resources/images/slides/slide-2.png);">
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="row m-0  slider-text-outer ">
                                <div class="slider-text">
                                    <div class="banner-content row m-0 justify-content-center">
                                        <div class="col-md-6">
                                            <h1>
                                                FOOD IN OUR LAND <br>
                                                HEALTH IN OUR HAND
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="section our-story">
        <div class="container">
            <div class="row align-items-center pb-5 boder-bottom">
                <div class="col-12  title ">
                    <h2 class="title-boxed">OUR STORY</h2>
                </div>
                <div class="col-lg-5 pr-4">
                    <p>How does a seed becomes a tree. This was the question asked by a kid while we were engaged in a
                        nature campaign. We all might know the scientific process behind it. But how could we explain it
                        to a four year kid in simple words. Nature reveal itself in simple forms- sprouts, roots,
                        leaves, stems, flowers..... Can we teach this dance of life to a kid. from this basic question
                        was born our new search - <span class="ft-medium"> Proleterian Eco Health Foundation </span>
                        with the motto <span class="ft-medium"> Food in our land.
                            Health in our hand. </span> Its our journey towards truth, the truth revealed by nature.
                        Through every
                        step we make we try to get in tune with cosmic dance. </p>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="resources/images/the-story.png" alt="" class="img-fluid ">
                </div>
                <div class="col-lg-3">
                    <blockquote>
                        <img src="resources/images/icons/quoate.svg" alt="">
                        <span class="w-100"> Sitting quietly doing nothing <br> Spring comes and the grass grows by
                            itself <br> Basho (Zen poet)</span>
                    </blockquote>
                </div>
            </div>
        </div>
    </section>
    <section class="objectives bg-right mb-md-5 pb-3">
        <div class="container pb-5 boder-bottom p-bt-85">
            <div class=" row  col-lg-9 m-0 obj-row   ">
                <div class="col-12 p-0  title  title-tail mb-2">
                    <h2 class="title-boxed">OBJECTIVES</h2>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/farming-and-health-study.svg" alt="">
                    </div>
                    <h4> FARMING AND HEALTH STUDY</h4>
                    <p>To propagate Eco centric or life centric Farming, education and health <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/natural-training-and-camps.svg" alt="">
                    </div>
                    <h4>NATURAL TRAINING AND CAMPS </h4>
                    <p>Plan and help create model eco farms and give training and conduct <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/soil-water-and-air.svg" alt="">
                    </div>
                    <h4> SOIL WATER AND AIR MANAGING</h4>
                    <p>Evolve methods to preserve soil, Water and Air and ensure healthy living to  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/back-to-education.svg" alt="">
                    </div>
                    <h4>BACK TO EDUCATION </h4>
                    <p>Focus on preprimary and primary education and try to evolve an  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/seeds-for-future.svg" alt="">
                    </div>
                    <h4>SEEDS FOR FUTURE </h4>
                    <p> Conserve local seed varieties and distribute among the farmers  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/children-magzine.svg" alt="">
                    </div>
                    <h4>CHILDRENS MAGAZINE </h4>
                    <p>Start a children's magazine which will be mouthpiece of the organization,  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/energy.svg" alt="">
                    </div>
                    <h4>NATURE AND ENERGY </h4>
                    <p>Help people to become self reliant in all segments of life including alternative  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/classes-and-symposiums.svg" alt="">
                    </div>
                    <h4>CLASSES AND SYMPOSIUMS </h4>
                    <p> Conduct classes and symposiums to propagate the slogan "healthy soil is  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/true-aggricultre-learning.svg" alt="">
                    </div>
                    <h4>TRUE AGRICULTURAL LEARNING </h4>
                    <p> Expose futile technology and Systems that are harmful to the nature and  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/film-making.svg" alt="">
                    </div>
                    <h4> FILM MAKING</h4>
                    <p> Start film clubs in the schools and help students to get hands on experience  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4 col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/ideology.svg" alt="">
                    </div>
                    <h4>ENVIRONMENT AND IDEOLOGY </h4>
                    <p>Weave together the harmonious elements of Marxian ideology along  <a href="#"
                            class="view-more">+</a></p>
                </div>
                <div class="col-lg-4  col-sm-6 obj-content">
                    <div class="ico-container">
                        <img src="resources/images/icons/nature-travel-and-study.svg" alt="">
                    </div>
                    <h4>NATURE TRAVEL AND STUDY </h4>
                    <p>Start nature clubs and conduct study tours to be closely in touch with our  <a href="#"
                            class="view-more">+</a></p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row gallery-cluster border-bottom pb-5 mb-5">
                <div class="col-12  title p-md-0 mb-4">
                    <h2 class="title-boxed ">GALLERY</h2>
                </div>
                <div class=" col-lg-4  col-sm-4 p-1">
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> NATUREʼS PROSPIRITY</h4>
                        </a>
                        <img src="resources/images/natural-prospirity.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4>AGRI WORKSHOP</h4>
                        </a>
                        <img src="resources/images/agri-workshop.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class=" col-lg-4 col-sm-4 p-1">
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> NATUECO FARMING</h4>
                        </a>
                        <img src="resources/images/natueco-farming.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> ETHNIC SEEDS</h4>
                        </a>
                        <img src="resources/images/ethnic-seeds.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> CREATIVE LEARNING</h4>
                        </a>
                        <img src="resources/images/creative-learning.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 p-1">
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> FOOD & HEALTH </h4>
                        </a>
                        <img src="resources/images/food-and-health.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> ECO HOMES </h4>
                        </a>
                        <img src="resources/images/eco-homes.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <div class="play-video">
                            <a class="js-play" href="#" data-toggle="modal" data-target="#modalVideo"
                                data-src="ZTuBRAxt72o" data-title="Earth Elemental Healing"> <img
                                    src="resources/images/icons/play.svg" alt=""></a>
                        </div>
                        <img src="resources/images/vid.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
    </section>
    <section class="activities">
        <div class="container">
            <div class="row">
                <div class="col-12  title p-md-0 mb-4">
                    <h2 class="title-boxed mb-4">NATURE & ACTIVITIES</h2>
                </div>
                <div class="row activites-row">
                    <div class=" col-lg-4 col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-1.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Latest event story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story</p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-2.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Bajwada Krishi teerth Story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4   col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>SEPT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-3.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3> Lakshadeep </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4   col-md-6 ">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-1.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Latest event story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story</p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-2.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Bajwada Krishi teerth Story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>

                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>SEPT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-3.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3> Lakshadeep </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <a id="loadMore"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                    <a id="showLess"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                </div>
            </div>
        </div>
    </section>
    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- /video modal/ -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modalVideo">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title js-title-insert"></h5>
                    <button class="close js-pause" type="button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body px-0 py-0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item" id="youTubeIframe"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/jquery.flexslider-min.js" type="text/javascript"></script>
    <script src="https://www.youtube.com/iframe_api" type="text/javascript"></script>
    </script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
        var sliderMain = function () {
            $('.hero .flexslider').flexslider({
                animation: "fade",
                //easing: "swing",
                // direction: "vertical",
                slideshowSpeed: 7000,
                directionNav: true,
                start: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                },
                before: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                }
            });
        };
        $(function () {
            sliderMain();
        });
        /* blog */
        var numberOfItems = 3;
        $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
        $('#loadMore').click(function () {
            numberOfItems = numberOfItems + 6;
            $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
            $('#loadMore').css("display", "none");
            $('#showLess').css("display", "block");
        });
        $('#showLess').click(function () {
            numberOfItems = numberOfItems - 6;
            $('.activities .activity-content-grid').not(':lt(' + numberOfItems + ')').hide();
            $('#loadMore').css("display", "block");
            $('#showLess').css("display", "none");
        });
        var player;
        var lastButton = '';
        const youtube = 'youTubeIframe';
        const titleInsert = '.js-title-insert';
        const btnPlay = '.js-play';
        const btnPause = '.js-pause';
        const modalId = '#modalVideo';
        const videoQuality = 'hd720';
        function onYouTubePlayerAPIReady() {
            player = new YT.Player(youtube, {
                controls: 2,
                iv_load_policy: 3,
                rel: 0,
                events: {
                    onReady: onPlayerReady
                }
            });
        }
        function onPlayerReady(event) {
            'use strict';
            $(btnPlay).on('click', function () {
                var videoId = $(this).attr('data-src');
                if (lastButton == videoId) {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.playVideo(videoId, 0, videoQuality);
                } else {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.loadVideoById(videoId, 0, videoQuality);
                    lastButton = videoId;
                }
            });
            $(btnPause).on('click', function () {
                player.pauseVideo();
            });
            $(modalId).on('click', function () {
                player.pauseVideo();
            });
        }
    </script>
</body>
</html>
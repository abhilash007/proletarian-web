<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Gallery </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>
<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="banner-outer  gallery-banner">
        <div class="hero">
        <div class="container">
        <div class="gallery-title">
             <h2 class="title-boxed  white-theme">GALLERY</h2>
        </div>
        </div>
            <div class="flexslider">
                <ul class="slides ">
                    <li style="background-image: url(resources/images/gallery-banner.png);"> </li>
                    <li style="background-image: url(resources/images/gallery-banner.png);"> </li>
                </ul>
            </div>
        </div>
    </section>


    <section class="gallery-holder pt-5">
        <div class="container">

            <div class="row gallery-cluster border-bottom pb-5 mb-5">
                <div class="col-12  title p-md-0 mb-4">
                    <h2 class="title-boxed ">GALLERY</h2>
                </div>
                <div class=" col-lg-4  col-sm-4 p-1">
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> NATUREʼS PROSPIRITY</h4>
                        </a>
                        <img src="resources/images/natural-prospirity.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4>AGRI WORKSHOP</h4>
                        </a>
                        <img src="resources/images/agri-workshop.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class=" col-lg-4 col-sm-4 p-1">
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> NATUECO FARMING</h4>
                        </a>
                        <img src="resources/images/natueco-farming.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="natures-prospirity.php" class="content">
                            <h4> ETHNIC SEEDS</h4>
                        </a>
                        <img src="resources/images/ethnic-seeds.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="#" class="content">
                            <h4> CREATIVE LEARNING</h4>
                        </a>
                        <img src="resources/images/creative-learning.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 p-1">
                    <div class="segment">
                        <a href="#" class="content">
                            <h4> FOOD & HEALTH </h4>
                        </a>
                        <img src="resources/images/food-and-health.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <a href="#" class="content">
                            <h4> ECO HOMES </h4>
                        </a>
                        <img src="resources/images/eco-homes.png" class="img-fluid" alt="">
                    </div>
                    <div class="segment">
                        <div class="play-video">
                            <a class="js-play" href="#" data-toggle="modal" data-target="#modalVideo"
                                data-src="ZTuBRAxt72o" data-title="Earth Elemental Healing"> <img
                                    src="resources/images/icons/play.svg" alt=""></a>
                        </div>
                        <img src="resources/images/vid.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
    </section>
    <section class="activities">
        <div class="container">
            <div class="row">
                <div class="col-12  title p-md-0 mb-4">
                    <h2 class="title-boxed mb-4">NATURE & ACTIVITIES</h2>
                </div>
                <div class="row activites-row">
                    <div class=" col-lg-4 col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-1.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Latest event story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story</p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-2.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Bajwada Krishi teerth Story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4   col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>SEPT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-3.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3> Lakshadeep </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4   col-md-6 ">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-1.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Latest event story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story</p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>OCT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-2.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3>Bajwada Krishi teerth Story </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>

                    <div class=" col-lg-4  col-md-6">
                        <a href="#" class=" truncate activity-content-grid">
                            <div class="img-wrapper ">
                                <div class="activity-date">
                                    <span>SEPT '18</span>
                                </div>
                                <div class="featured-image "
                                    style="background-image:url(resources/images/activity-3.png)">
                                </div>
                            </div>
                            <div class="event-text ">
                                <div class="comment-count row justify-content-end">
                                    <p>2 comments </p> <span><img src="resources/images/icons/comment.svg" alt="">
                                    </span>
                                </div>
                                <h3> Lakshadeep </h3>
                                <p>About Bajwada Krishi Teerth event story About Bajwada Krishi Teerth event storyAbout
                                    Bajwada Krishi Teerth event storyAbout Bajwada Krishi Teerth event storyAbout
                                    Bajwada
                                    Krishi Teerth event story </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <a id="loadMore"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                    <a id="showLess"><img src="resources/images/icons/arrow-down.svg" alt=""> </a>
                </div>
            </div>
        </div>
    </section>
    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- /video modal/ -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modalVideo">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title js-title-insert"></h5>
                    <button class="close js-pause" type="button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body px-0 py-0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item" id="youTubeIframe"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/jquery.flexslider-min.js" type="text/javascript"></script>
    <script src="https://www.youtube.com/iframe_api" type="text/javascript"></script>
    </script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
        var sliderMain = function () {
            $('.hero .flexslider').flexslider({
                animation: "fade",
                //easing: "swing",
                // direction: "vertical",
                slideshowSpeed: 7000,
                directionNav: true,
                start: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                },
                before: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                }
            });
        };
        $(function () {
            sliderMain();
        });
        /* blog */
        var numberOfItems = 3;
        $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
        $('#loadMore').click(function () {
            numberOfItems = numberOfItems + 6;
            $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
            $('#loadMore').css("display", "none");
            $('#showLess').css("display", "block");
        });
        $('#showLess').click(function () {
            numberOfItems = numberOfItems - 6;
            $('.activities .activity-content-grid').not(':lt(' + numberOfItems + ')').hide();
            $('#loadMore').css("display", "block");
            $('#showLess').css("display", "none");
        });
        var player;
        var lastButton = '';
        const youtube = 'youTubeIframe';
        const titleInsert = '.js-title-insert';
        const btnPlay = '.js-play';
        const btnPause = '.js-pause';
        const modalId = '#modalVideo';
        const videoQuality = 'hd720';
        function onYouTubePlayerAPIReady() {
            player = new YT.Player(youtube, {
                controls: 2,
                iv_load_policy: 3,
                rel: 0,
                events: {
                    onReady: onPlayerReady
                }
            });
        }
        function onPlayerReady(event) {
            'use strict';
            $(btnPlay).on('click', function () {
                var videoId = $(this).attr('data-src');
                if (lastButton == videoId) {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.playVideo(videoId, 0, videoQuality);
                } else {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.loadVideoById(videoId, 0, videoQuality);
                    lastButton = videoId;
                }
            });
            $(btnPause).on('click', function () {
                player.pauseVideo();
            });
            $(modalId).on('click', function () {
                player.pauseVideo();
            });
        }
    </script>
</body>
</html>
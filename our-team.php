<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Our Team </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>

<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/our-team-banner.png)">
        <div class="container">
            <h1 class="title-boxed">OUR TEAM </h1>
        </div>
        </div>
    </section>

    <section class="section team-members mt-5 pt-5">
        <div class="container">
            <div class="row  pb-5 boder-bottom">
                <div class="col-12  title pb-4">
                    <h2 class="title-boxed">CHIEF ADVISOR</h2>
                </div>
                <div class="col-lg-2 member ">
                    <img src="resources/images/team/team-member-1.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid chief">
                    <h6>Dr. K.P Prabhakaran Nair</h6>
                </div>
                <div class="col-lg-5 ">
                    <p>Kodoth Prabhakaran Nair is a globally acclaimed agricultural scientist, having worked in
                        different capacities in Europe, Africa and Asia, for over three decades. A former Rockefeller
                        Fellow, and a Senior Alexander von Humboldt Fellow, of The Federal Republic of Germany, he was
                        the first and only Asian scientist named to the very prestigious National Chair of the Science
                        Foundation, The Royal Society, Belgium. Recipient of several international and national awards,
                    </p>
                </div>

                <div class="col-lg-5">
                    <p>including the Swadeshi Sastra Puraskar of the Swadeshi Science Movement of the Government of
                        India, Involved in international science, in an honorary capacity, he is best known globally for
                        developing an original and revolutionary soil management concept, now known as “The Nutrient
                        Buffer Power Concept”, and is invited to several international conferences, workshops and
                        advisory panels</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section team-members mt-5 pt-5">
        <div class="container">
            <div class="row  pb-5 boder-bottom">
                <div class="col-12  title pb-4 mb-4">
                    <h2 class="title-boxed">ADVISORY MEMBERS</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-2.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>Dr. RAJAN GURUKKAL</h5>
                    <p>HIGHER EDUCATION COUNCIL CHAIRMAN, KERALA STATE.</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-3.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <p class="m-0 p-0 pt-2">PADMABHUSHAN</p>
                    <h5 class="m-0">Dr. B M HEGDE</h5>

                    <p>MD, PH.D, FRCP (LOND. EDIN.GLASG& DUBLIN) FACC, FAMS.</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-4.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>DEEPAK SUCHDE</h5>
                    <p>NATUECO SCIENTIST, DEWAS, MADYAPRADESH</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-5.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>Dr. J G RAY</h5>
                    <p>M.SC, ME&E, PH.D, DIRECTOR, SCHOOL OF BIO SCIENCE, M G UNIVERSITY. CO-ORDINATOR, NATIONAL
                        INSTITUTE OF PLANT TECHNOLOGY</p>
                </div>

            </div>
        </div>
    </section>

    <section class="section team-members mt-5 pt-5">
        <div class="container">
            <div class="row  pb-5 boder-bottom">
                <div class="col-12  title pb-4 mb-4">
                    <h2 class="title-boxed">ADVISORY MEMBERS</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-6.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>Dr. PRABHAKAR <br>KULKARNI PH.D </h5>
                    <p>IN BIOTECHNOLOGY RESEARCH & DEVELOPMENT, MOLECULAR BIOLOGY (DNA STABILITY STUDIES)</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-7.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>Dr. DINISH U.S PH.D </h5>
                    <p>IN BIOTECHNOLOGY RESEARCH & DEVELOPMENT, MOLECULAR BIOLOGY (DNA STABILITY STUDIES)</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-8.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5> Dr. GIRISH KUMAR P</h5>
                    <p>ZOOLOGICAL ASSISTANT, ZOOLOGICAL SURVEY OF INDIA,KOLKATA.</p>
                </div>


            </div>
        </div>
    </section>

    <section class="section team-members mt-5 pt-5">
        <div class="container">
            <div class="row  pb-5 boder-bottom">
                <div class="col-12  title pb-4 mb-4">
                    <h2 class="title-boxed"> FOUNDERS</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member pl-3 ">
                    <img src="resources/images/team/team-member-10.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5> SHABIL KRISHNAN</h5>
                    <p>AGRI CORRESPONDANTAND NATUECO TRAINER</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-11.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>VIVEK A </h5>
                    <p>SOIL MICROBIOLOGY RESEARCH SCIENTIST</p>


                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member  ">
                    <img src="resources/images/team/team-member-12.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>Dr. ASLAM FAROOK Z A </h5>
                    <p>M B B S, M D, HEALTH CARE. (GOVT. TALUK HOSPITAL, PERAMBRA)</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member  ">
                    <img src="resources/images/team/team-member-9.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5> Dr. KAVITHA RAMAN</h5>
                    <p>TRADITIONAL AYURVEDA(ASST.PROFESSOR, SREE SANKARACHARYA UNIVERSITY.)</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-13.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5> Dr. SHAHINA K RAFEEQ</h5>
                    <p>WRITER AND FREE LANCE JOURNALSIT, (PH.D IN FILM STUDIES)</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-14.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5> BIJESH K K</h5>
                    <p>NATUECO FARMER AND TRAINER</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-15.png" alt="Dr. K.P Prabhakaran Nair"
                        class="w-100 img-fluid">
                    <h5>KISHOR KUMAR </h5>
                    <p>HEALTH AND FOOD (FORMER INDIAN VOLLEYBALL TEAM CAPTAIN, HUMAN RESOURCE OFFICER, B P C L, COCHIN)
                    </p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member  ">
                    <img src="resources/images/team/team-member-16.png" alt="" class="w-100 img-fluid">
                    <h5> SHIJU L N </h5>
                    <p>PLANNING WING(SECRETARY, GRAMA PANCHAYAT)</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section team-members mt-5 pt-5">
        <div class="container">
            <div class="row  pb-5 boder-bottom">
                <div class="col-12  title pb-4 mb-4">
                    <h2 class="title-boxed">WORKING TEAM</h2>
                </div>



                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-17.png" alt="" class="w-100 img-fluid">
                    <h5> ANCY N S</h5>
                    <p>MA, B.Ed. CHILDRENʼS EDUCATION</p>


                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-18.png" alt="" class="w-100 img-fluid">
                    <h5> RANJISH RAGHAVAN </h5>
                    <p>NATUECO FARMER, TRAINER AND SEED PRESERVER</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member ">
                    <img src="resources/images/team/team-member-19.png" alt="" class="w-100 img-fluid">
                    <h5>NITANTH L RAJ </h5>
                    <p>EDITOR, CHILDRENʼS MAGAZINE</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-20.png" alt="" class="w-100 img-fluid">
                    <h5>GEETHU NARAYAN </h5>
                    <p>MA, B.Ed. PROJECT CO-ORDINATOR</p>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-21.png" alt="" class="w-100 img-fluid">
                    <h5> SANUSH KUMAR </h5>
                    <p>MSC, B Ed. CENTRAL UNIVERSITY OF KERALA, KASARAGOD</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member  ">
                    <img src="resources/images/team/team-member-22.png" alt="" class="w-100 img-fluid">
                    <h5> SHINJU K CHANDRAN</h5>
                    <p>MECHANICAL ENGINEEROMAN</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 member">
                    <img src="resources/images/team/team-member-23.png" alt="" class="w-100 img-fluid">
                    <h5>AJAY GOVARDHAN </h5>
                    <p>CREATIVE DIRECTOR</p>
                </div>
            </div>
        </div>
    </section>

    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- /video modal/ -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modalVideo">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title js-title-insert"></h5>
                    <button class="close js-pause" type="button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body px-0 py-0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item" id="youTubeIframe"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/jquery.flexslider-min.js" type="text/javascript"></script>
    <script src="https://www.youtube.com/iframe_api" type="text/javascript"></script>
    </script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
    <script>
        var sliderMain = function () {
            $('.hero .flexslider').flexslider({
                animation: "fade",
                //easing: "swing",
                // direction: "vertical",
                slideshowSpeed: 7000,
                directionNav: true,
                start: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                },
                before: function () {
                    setTimeout(function () {
                        $('.slider-text').removeClass('animated fadeInUp');
                        $('.flex-active-slide').find('.slider-text').addClass(
                            'animated fadeInUp');
                    }, 600);
                }
            });
        };
        $(function () {
            sliderMain();
        });
        /* blog */
        var numberOfItems = 3;
        $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
        $('#loadMore').click(function () {
            numberOfItems = numberOfItems + 6;
            $('.activities .activity-content-grid:lt(' + numberOfItems + ')').show();
            $('#loadMore').css("display", "none");
            $('#showLess').css("display", "block");
        });
        $('#showLess').click(function () {
            numberOfItems = numberOfItems - 6;
            $('.activities .activity-content-grid').not(':lt(' + numberOfItems + ')').hide();
            $('#loadMore').css("display", "block");
            $('#showLess').css("display", "none");
        });
        var player;
        var lastButton = '';
        const youtube = 'youTubeIframe';
        const titleInsert = '.js-title-insert';
        const btnPlay = '.js-play';
        const btnPause = '.js-pause';
        const modalId = '#modalVideo';
        const videoQuality = 'hd720';

        function onYouTubePlayerAPIReady() {
            player = new YT.Player(youtube, {
                controls: 2,
                iv_load_policy: 3,
                rel: 0,
                events: {
                    onReady: onPlayerReady
                }
            });
        }

        function onPlayerReady(event) {
            'use strict';
            $(btnPlay).on('click', function () {
                var videoId = $(this).attr('data-src');
                if (lastButton == videoId) {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.playVideo(videoId, 0, videoQuality);
                } else {
                    $(titleInsert).text($(this).attr('data-title'));
                    player.loadVideoById(videoId, 0, videoQuality);
                    lastButton = videoId;
                }
            });
            $(btnPause).on('click', function () {
                player.pauseVideo();
            });
            $(modalId).on('click', function () {
                player.pauseVideo();
            });
        }
    </script>
</body>

</html>
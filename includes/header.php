<div class="preloader">

    <div class="preloader-logo">

        <img src="resources/images/fav.png">

        <!-- <div id="spinner"></div> -->

    </div>
</div>
<div class="page-content">
    <header class="page-header">
        <div class="navigation-holder">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light  ">
                    <a class="navbar-brand" href="index.php"><img src="resources/images/logo.png"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                        <div class="row col-md-11 justify-content-lg-end">
                            <ul class="navbar-nav fill ">


                                <li class="nav-item dropdown">
                                     <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">OUR STORY</a>
                                     <ul class="dropdown-menu">
                                         <li class=" nav-item">
                                             <a class="page-scroll nav-link" href="">Objectives</a>
                                         </li>
                                         <li class=" nav-item">
                                             <a class="page-scroll nav-link" href="vision.php">Vision</a>
                                         </li>

                                     </ul>
                                 </li>

                                <li class="nav-item ">
                                    <a class="nav-link" href="our-team.php">TEAM </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="gallery.php"> GALLERY</a>
                                </li>

                                <li class="nav-item dropdown">
                                     <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">BOOKS</a>
                                     <ul class="dropdown-menu">
                                         <li class=" nav-item">
                                             <a class="page-scroll nav-link" href="books.php">Books</a>
                                         </li>
                                         <li class=" nav-item">
                                             <a class="page-scroll nav-link" href="magazines.php">New papers & Magazine</a>
                                         </li>

                                     </ul>
                                 </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="news-and-events.php">NEWS & EVENTS </a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link" href="donate-now.php"> DONATE NOW</a>
                                </li>
                                <li class="nav-item search-box">
                                    <form class="form">
                                    <label class="f-label" for="search">Search</label>
                                    <input type="text" name="search" class="form-feild"   placeholder=""   />
                                    <button class="search"><span class=""> <img
                                                src="resources/images/icons/search-icon.svg" alt=""> </span></button>
                                                </form>
                              </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
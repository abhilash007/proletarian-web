<footer class="page-footer ">
    <div class="container">
        <div class="row  ">
            <div class="col-lg-4 col-sm-6 address ">
            <h5>CONTACT US </h5>
                <p class="text-justify concise"></p>


                <h6>Proletarian Eco Health Research Foundation</h6>
               <p class="mb-0">Reg.No : 5/2013/4, Kalloth , Build.No:5 Chenoli Po, Perambra, Kozhikode, 673525.</p>

              <a class="contact-num" href="">Phone: 0496 2613178, +91- 9497303178 , 9446405636</a>
              <a class="contact-eamil" href="">Email: ecohealthfoundation@gmail.com</a>
            </div>
            <div class="col-lg-2 col-sm-6">
            <h5 >RESOURCES</h5>
                <ul class="quick-links row  flex-column">
                    <li><a href="books.php"> Books </a></li>
                    <li><a href="magazines.php"> Magazines </a></li>
                    <li><a href="gallery.php"> Gallery </a></li>
                    <li><a href=""> Video gallery </a></li>


                </ul>
            </div>
            <div class="col-lg-2 col-sm-6">
                <h5>LATEST</h5>
                <ul class="quick-links row  flex-column">

                    <li><a href="#" target="_blank"> News </a></li>
                    <li><a href="#" target="_blank">Campaigns </a></li>
                    <li><a href="#" target="_blank">Research</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h5>BE A VOLUNTERE</h5>
                <p>If you are talented and passionate about human rights then Proletarian Eco Health Research Foundationwants to hear from you</p>
                <a class="join" href="join-as-volunteer.php" >JOIN AS VOLUNTEER</a>
            </div>
        </div>
    </div>

</footer>
<a href="javascript:" id="return-to-top">  <img src="resources/images/icons/arrow-up.svg" alt=""> </a>

</div>

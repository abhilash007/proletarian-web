function loaderStrip() {
    $(window).on("load", function () {
        var $box = $('.preloader-logo');
        $box.addClass('horizTranslate');
        $('.preloader').addClass('gone');
        setTimeout(function () {
            $('.page-content').addClass('here');
            $('.legend').addClass('fade-in');
        }, 300);
    });
}
loaderStrip();
//header animation
$(window).scroll(function (e) {
    var $element = $('.page-header');
    var scrollTop = $(this).scrollTop();
    if (scrollTop <= 0) {
        $element.removeClass('hide').removeClass('scrolling');
    } else if (scrollTop < position) {
        $element.removeClass('hide');
    } else if (scrollTop > position) {
        $element.addClass('scrolling');
        if (scrollTop + $(window).height() >= $(document).height() - $element.height()) {
            $element.removeClass('hide');
        } else if (Math.abs($element.position().top) < $element.height()) {
            $element.addClass('hide');
        }
    }
    position = scrollTop;
})
var position = 0;
function fixMargin() {
    var headerHeight = $(".page-header").height();
    $(".push-top").css('margin-top', headerHeight);
}
fixMargin();
var path = window.location.pathname.split("/").pop();
if (path == '') {
    path = 'index';
}
var target = $('.navbar-nav li a[href="' + path + '"]');
target.addClass('active');



if (window.matchMedia('(min-width: 768px)').matches) {

    $('.navbar-nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

}

$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

jQuery(document).ready(function () {
    var formControl = jQuery(' .form ').find('.form-feild ');
    var inputElem = jQuery('.form ').find('.form-feild ');
    inputElem.focus(function () {
        jQuery(this).parent().find('.f-label').addClass("has-value");
    });
    inputElem.blur(function () {
        jQuery(this).parent('.form').removeClass('focus');
    });
    formControl.each(function () {
        var targetItem = jQuery(this).parent();
        if (jQuery(this).val()) {
            jQuery(targetItem).addClass('has-value');
        }
    });
    formControl.blur(function () {
        jQuery(this).parent('.form').removeClass('focus');
        if (jQuery(this).val().length == 0) {
            jQuery(this).parent().find('.f-label').removeClass("has-value");
        } else {
            jQuery(this).parent().find('.f-label').addClass("has-value");
        }
    });
});
function init() {
    document.getElementsByClassName("input").value = "";
}
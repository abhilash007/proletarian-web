<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | NATUREʼS PROSPIRITY </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/darkbox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>

<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/gallery-banner.png)">
        <div class="container">
            <div class="content">
                <h1 class="title-boxed white-theme">GALLERY</h1>
            </div>
        </div>
        </div>
    </section>
    <div class="gallery-grid gallery-feeds pb-5 pt-5">
        <div class="container">
            <div class="col-12  title p-md-0 mb-4">
                <h2 class="title-boxed mb-4">NATUREʼS PROSPIRITY</h2>
            </div>
            <div class="row">

                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-1.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-2.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-3.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-4.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-5.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-6.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-7.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-8.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="gallery-single.php" class="img-holder">
                        <figure class="snip hover-item"
                            style="background-image:url(resources/images/gallery/natures-prospirity/img-9.png)">
                            <figcaption>
                                <div class="symbol">
                                    <i class="maximize"> <img src="resources/images/maximize.svg" alt=""> </i> </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>



            </div>
            <div class="row justify-content-center pt-4 pb-4">

                <nav data-pagination>
                    <a href=# disabled><i class="icofont-thin-left"></i></a>
                    <ul>
                        <li class=current><a href=#1>1</a>
                        <li><a href=#2>2</a>
                        <li><a href=#3>3</a>
                        <li><a href=#4>4</a>
                        <li><a href=#5>5</a>
                    </ul>
                    <a href=#2><i class="icofont-thin-right"></i></a>
                </nav>

            </div>
        </div>
    </div>
    <section class="motto" style="background-image:url(resources/images/Web_33.png)">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-lg-4 text-center text-md-left">
                    <img src="resources/images/food-logo.svg" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <h5>
                        A SMALL BODY OF
                        DETERMINED SPIRITS FIRED BY
                        AN UNQUENCHABLE
                        FAITH IN THEIR MISSION CAN
                        ALTER THE COURSE OF
                        HISTORY.
                    </h5>
                    <h6> MAHATMA GANDHI</h6>
                </div>
            </div>
        </div>
    </section>

    <!-- / footer / -->
    <?php include 'includes/footer.php'; ?>
    <!--    /javascripts/    -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/darkbox.js" type="text/javascript"></script>
    <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
</body>

</html>
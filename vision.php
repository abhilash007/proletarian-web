<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Proletarian | Vision </title>
    <!-- MOBILE -->
    <meta name='HandheldFriendly' content='true' />
    <meta name='format-detection' content='telephone=no' />
    <meta name="apple-mobile-web-app-title" content=" " />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- / common / -->
    <meta name="author" content="  ">
    <meta name="keywords" content="">
    <meta name="description" content="" />
    <!-- FB -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="resources/images/share.png">
    <meta property="og:url" content="">
    <!-- TWITTER  -->
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="resources/images/share.png">
    <meta name="twitter:card" content="summary_large_image">
    <!--  /for analytics/ -->
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@website-username">
    <!-- fav Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="resources/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="resources/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="resources/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <!-- <link rel="stylesheet" type="text/css" href="resources/fonts/icofont/icofont.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css?v=1.0">
</head>

<body>
    <!-- / header / -->
    <?php include 'includes/header.php'; ?>
    <!--    /banner/  -->
    <section class="page-banner" style="background-image:url(resources/images/vision-banner.png)">
        <div class="container">
            <div class="content">
            <h1 class="title-boxed white-theme"> VISION </h1>
            <div class="col-lg-6 p-0 mt-3">
                <p>We believe that our task of spreading the knowledge is made easy when we focus on our kids because
                    they are less polluted and less prejudiced. Our proposed children's magazine is also the mouthpiece
                    of our organization. Through various innovative methods we wish to propagate our philosophy of
                    harmony to kids. This is the only solution to heal the age old wounds of humanity.</p>
            </div>
            </div>
        </div>
        </div>
        </section>
        <section class="section our-story">
            <div class="container">
                <div class="row align-items-center pb-5 boder-bottom">
                    <div class="col-12  title ">
                        <h2 class="title-boxed">OUR STORY</h2>
                    </div>
                    <div class="col-lg-5 pr-4">
                        <p>How does a seed becomes a tree. This was the question asked by a kid while we were engaged in
                            a
                            nature campaign. We all might know the scientific process behind it. But how could we
                            explain it
                            to a four year kid in simple words. Nature reveal itself in simple forms- sprouts, roots,
                            leaves, stems, flowers..... Can we teach this dance of life to a kid. from this basic
                            question
                            was born our new search - <span class="ft-medium"> Proleterian Eco Health Foundation </span>
                            with the motto <span class="ft-medium"> Food in our land.
                                Health in our hand. </span> Its our journey towards truth, the truth revealed by nature.
                            Through every
                            step we make we try to get in tune with cosmic dance. </p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <img src="resources/images/the-story.png" alt="" class="img-fluid ">
                    </div>
                    <div class="col-lg-3">
                        <blockquote>
                            <img src="resources/images/icons/quoate.svg" alt="">
                            <span class="w-100"> Sitting quietly doing nothing <br> Spring comes and the grass grows by
                                itself <br> Basho (Zen poet)</span>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <section class="objectives bg-right mb-md-5 pb-3">
            <div class="container pb-5 boder-bottom p-bt-85">
                <div class=" row  col-lg-9 m-0 obj-row pr-md-5  ">
                    <div class="col-12 p-0  title  title-tail mb-2">
                        <h2 class="title-boxed">OBJECTIVES</h2>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/farming-and-health-study.svg" alt="">
                        </div>
                        <h4> FARMING AND HEALTH STUDY</h4>
                        <p>To propagate Eco centric or life centric Farming, education and health <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/natural-training-and-camps.svg" alt="">
                        </div>
                        <h4>NATURAL TRAINING AND CAMPS </h4>
                        <p>Plan and help create model eco farms and give training and conduct <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/soil-water-and-air.svg" alt="">
                        </div>
                        <h4> SOIL WATER AND AIR MANAGING</h4>
                        <p>Evolve methods to preserve soil, Water and Air and ensure healthy living to <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/back-to-education.svg" alt="">
                        </div>
                        <h4>BACK TO EDUCATION </h4>
                        <p>Focus on preprimary and primary education and try to evolve an <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/seeds-for-future.svg" alt="">
                        </div>
                        <h4>SEEDS FOR FUTURE </h4>
                        <p> Conserve local seed varieties and distribute among the farmers <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/children-magzine.svg" alt="">
                        </div>
                        <h4>CHILDRENS MAGAZINE </h4>
                        <p>Start a children's magazine which will be mouthpiece of the organization, <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/energy.svg" alt="">
                        </div>
                        <h4>NATURE AND ENERGY </h4>
                        <p>Help people to become self reliant in all segments of life including alternative <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/classes-and-symposiums.svg" alt="">
                        </div>
                        <h4>CLASSES AND SYMPOSIUMS </h4>
                        <p> Conduct classes and symposiums to propagate the slogan "healthy soil is <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/true-aggricultre-learning.svg" alt="">
                        </div>
                        <h4>TRUE AGRICULTURAL LEARNING </h4>
                        <p> Expose futile technology and Systems that are harmful to the nature and <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/film-making.svg" alt="">
                        </div>
                        <h4> FILM MAKING</h4>
                        <p> Start film clubs in the schools and help students to get hands on experience <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4 col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/ideology.svg" alt="">
                        </div>
                        <h4>ENVIRONMENT AND IDEOLOGY </h4>
                        <p>Weave together the harmonious elements of Marxian ideology along <a href="#"
                                class="view-more">+</a></p>
                    </div>
                    <div class="col-lg-4  col-sm-6 obj-content">
                        <div class="ico-container">
                            <img src="resources/images/icons/nature-travel-and-study.svg" alt="">
                        </div>
                        <h4>NATURE TRAVEL AND STUDY </h4>
                        <p>Start nature clubs and conduct study tours to be closely in touch with our <a href="#"
                                class="view-more">+</a></p>
                    </div>
                </div>
            </div>
        </section>


        <section class="motto" style="background-image:url(resources/images/Web_33.png)">
            <div class="container">
                <div class="row align-items-center justify-content-around">
                    <div class="col-lg-4 text-center text-md-left">
                        <img src="resources/images/food-logo.svg" alt="">
                    </div>
                    <div class="col-lg-4 text-center">
                        <h5>
                            A SMALL BODY OF
                            DETERMINED SPIRITS FIRED BY
                            AN UNQUENCHABLE
                            FAITH IN THEIR MISSION CAN
                            ALTER THE COURSE OF
                            HISTORY.
                        </h5>
                        <h6> MAHATMA GANDHI</h6>
                    </div>
                </div>
            </div>
        </section>

        <!-- / footer / -->
        <?php include 'includes/footer.php'; ?>
        <!--    /javascripts/    -->
        <script src="resources/js/jquery-3.3.1.min.js"></script>
        <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/js/custom-scripts.js?v=30" type="text/javascript"></script>
</body>

</html>